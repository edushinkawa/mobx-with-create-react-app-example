import { extendObservable, computed, action, autorun } from "mobx";
import axios from "axios";

class BandStore {
  constructor() {
    extendObservable(this, {
      bands: "",
      artists: [],
      searchParam: "",
      albums: [],
      search: action(this.search),
      loading: false,
      oauth: "",
      searchType: ""
      //getAlbumsQty: computed(() => `O número de álbums é ${this.albums.length}`)
    });
  }

  addBandsList(artists) {
    this.artists.push(artists);
  }

  search(artist, type="artist") {
    let self = this
    this.albums = []
    this.loading = true
    axios
      .get("https://api.spotify.com/v1/search", {
        params: {
          q: artist,
          type: type
        },
        headers: {
          Authorization: `Bearer ${this.oauth}`
        }
      })
      .then(function(response) {
        self.artists = response.data.artists.items
        self.playlist = response.data.playlist.items
        self.track = response.data.track.items
        self.album = response.data.album.items
      })
      .catch(function(error) {
        self.loading = false
      })
  }

  onChangeInput(e) {
    this.searchParam = e.target.value;
  }

  getAlbums(id) {
    let self = this;
    axios
      .get(`https://api.spotify.com/v1/artists/${id}/albums`)
      .then(function(response) {
        self.albums = response.data.items;
      });
  }
}

export default new BandStore();

var store = new BandStore();

autorun(() => {
  console.log(store);
});
