import React from "react"
import ReactDOM from "react-dom"
import App from "./App"

import BandStore from "./Stores/BandStore"

import "./index.css"

import { Provider } from "mobx-react"

const band = BandStore
const stores = { band }

ReactDOM.render(
	<Provider {...stores}>
		<App />
	</Provider>,
	document.getElementById("root")
)
