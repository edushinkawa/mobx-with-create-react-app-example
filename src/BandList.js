import React, { Component } from "react";
import { observer, inject } from "mobx-react";
import DevTools from "mobx-react-devtools";
import logo from "./logo.svg";
import "./App.css";
import "bulma/css/bulma.css";
import Modal from "react-modal";

class Results extends Component {
  onChangeInput(e) {
    this.props.band.searchParam = e.target.value;
  }
  handleChange(e) {
    console.log(e.target.value);
    //this.props.band.searchType = e.target.value
    this.props.band.search(this.props.band.searchParam, e.target.value);
  }
  render() {
    const { band } = this.props;

    return (
      <div>
        <select
          class="ctools-jump-menu-select ctools-jump-menu-change form-select ctools-jump-menu-processed"
          value={this.props.band.searchType}
          onChange={this.handleChange.bind(this)}
        >
          <option selected="selected" id="OPTION_2">
            - FILTER -
          </option>
          <option value="artist">
            Artist
          </option>
          <option value="album">
            Album
          </option>
          <option value="track">
            Track
          </option>
          <option value="playlist">
            playlist
          </option>
        </select>
        {band.artists !== undefined
          ? band.artists
              .filter((item, index) => index === 0)
              .map((item, index) => (
                <div style={{ textAlign: "center" }}>

                  <br /><br /><br />
                  {item.images
                    .filter((item, index) => index === 0)
                    .map((img, index) => (
                      <div className="results">
                        <p key={item.id}>{item.name}</p>
                        <img src={img.url} height="64" width="64" />

                      </div>
                    ))}
                  {item.images
                    .filter((item, index) => index === 0)
                    .map((img, index) => (
                      <div className="results">
                        <p key={item.id}>{item.name}</p>
                        <img src={img.url} height="64" width="64" />

                      </div>
                    ))}
                </div>
              ))
          : null}

      </div>
    );
  }
}

export default inject("band")(observer(Results));
