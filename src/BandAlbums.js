import React, { Component } from "react"
import { observer, inject } from "mobx-react"
import DevTools from "mobx-react-devtools"
import logo from "./logo.svg"
import "./App.css"
import "bulma/css/bulma.css"
import Modal from "react-modal"

class Albums extends Component {

  render() {
    const { band } = this.props

    return (
      <div>
        {band.albums !== undefined
          ? band.albums
              .map((item, index) => (
                <div>
                  {item.name} 
                </div>
              ))
          : null}
      </div>
    )
  }
}

export default inject("band")(observer(Albums))
