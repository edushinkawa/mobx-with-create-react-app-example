import React, { Component } from "react";
import { observer, inject } from "mobx-react";
import DevTools from "mobx-react-devtools";
import logo from "./logo.svg";
import "./App.css";
import "bulma/css/bulma.css";
import Modal from "react-modal";

class Search extends Component {
  onChangeInput(e) {
    this.props.band.onChangeInput(e);
  }

  onChangeInputOauth(e){
    this.props.band.oauth = e.target.value
  }

  render() {
    const { band } = this.props;
    const styleLoading = { marginLeft: 5, pointerEvents: null, opacity: 0.3 }
    const style = { marginLeft: 5 }

    return (
      <div className="Container">

        <h1>Spotify API React Test</h1>
          <input
            class="input"
            type="text"
            placeholder="OAUTH Code"
            onChange={this.onChangeInputOauth.bind(this)}
          /> <br /><br />
        <div style={{ display: "inline-flex" }}>
          <input
            class="input"
            type="text"
            placeholder="Looking for...?"
            onChange={this.onChangeInput.bind(this)}
          />
          <a
            onClick={() => band.search(band.searchParam)}
            className={band.loading ? "button is-danger is-outlined is-loading" : "button is-danger is-outlined"}
            style={band.loading ? styleLoading : style}
          >
            Search
          </a>
        </div>
      </div>
    );
  }
}

export default inject("band")(observer(Search));
