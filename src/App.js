import React, { Component } from "react"
import { observer, inject } from "mobx-react"
import DevTools from "mobx-react-devtools"
import logo from "./logo.svg"
import "./App.css"
import "bulma/css/bulma.css"
import Modal from "react-modal"
import BandList from "./BandList"
import BandAlbums from "./BandAlbums"
import Search from "./Search"

class App extends Component {
  onChangeInput(e) {
    band.onChangeInput(e)
  }

  render() {
    const { band } = this.props

    return (
      <div>
        <Search />
        <br /><br />
        {band.getAlbumsQty}
        <div className="columns container is-fluid">
          <div className="is-text-centered" style={{ width : "100%"}}>
            <BandList />
          </div>
        </div>
      </div>
    )
  }
}

export default inject("band")(observer(App))
